;;; eyebrowse-tab-bar.el --- Displays your eyebrowse tabs when switching window configs -*- lexical-binding: t -*-

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;;; Commentary:

;; This package adds a hook where on switching eyebrowse window configs, a list
;; of window configs appears in the echo area, showing the config name and the
;; active buffer name. The active config will be surrounded with square brackets
;; and highlighted.

;; An example tab bar might look like this:
;;   1: geddit.el   [2: init.el]   3: Transform.cpp

;;; Code:

(require 'eyebrowse)



(defun eyebrowse-tab-bar-get-tab-configs ()
  "Return a list of tab configs for the current frame."
  (let ((config-list '()))
    (dolist (layout (eyebrowse--get 'window-configs (selected-frame)))
      (push (eyebrowse-tab-bar--get-tab-info-from-config layout) config-list))

    ;; Return
    (reverse config-list)))

(defun eyebrowse-tab-bar--get-tab-info-from-config (layout)
  "Return a list where car is the tab index and cdr is the selected buffer's data from the given LAYOUT."
  (let* ((index (car layout))
         (data (cadr layout))
         (tab-name (eyebrowse-tab-bar--find-selected-buffer data)))

    ;; Return
    (cons index tab-name)))

(defun eyebrowse-tab-bar--find-selected-buffer (data)
  "Recursively search through the list DATA to find a buffer with (selected . t)."
  (catch 'found
    (dolist (item data)
      (when (listp item)
        (cond ((or (eq (car item) 'leaf)
                   (and (eq (car item) 'hc) (listp (cdr item))))
               (eyebrowse-tab-bar--throw-when-not-nil 'found (eyebrowse-tab-bar--find-selected-buffer (cdr item))))

              ((eq (car item) 'buffer)
               (eyebrowse-tab-bar--throw-on-p 'found item (eyebrowse-tab-bar--buffer-selected-p item))))))))

(defun eyebrowse-tab-bar--buffer-selected-p (buffer)
  "Return t if BUFFER has (selected . t), otherwise return nil."
  (catch 'found
    (dolist (item (cddr buffer))
      (when (eq (car item) 'selected)
        (throw 'found (cdr item))))))

(defun eyebrowse-tab-bar--throw-when-not-nil (tag value)
  "Throw TAG with value if VALUE is not nil."
  (when value
    (throw tag value)))

(defun eyebrowse-tab-bar--throw-on-p (tag value predicate)
  "Throw TAG with VALUE if PREDICATE is not nil."
  (when predicate
    (throw tag value)))

(defun eyebrowse-tab-bar--post-window-switch ()
  "Display the tab list and active tab in the echo area."
  (let ((tab-configs (eyebrowse-tab-bar-get-tab-configs))
        (out-str "")
        (current-tab (eyebrowse--get 'current-slot)))

    (dolist (tab tab-configs)
      (if (eq (car tab) current-tab)
          (setq out-str (concat out-str (propertize
                                         (concat " [" (number-to-string (car tab)) ": " (caddr tab) "] ")
                                         'face '(:foreground "deep sky blue"))))
        (setq out-str (concat out-str "  " (number-to-string (car tab)) ": " (caddr tab) "  "))))

    (message out-str)))

;; Register the tab bar display thingy when you switch layout
(add-to-list 'eyebrowse-post-window-switch-hook 'eyebrowse-tab-bar--post-window-switch)

(provide 'eyebrowse-tab-bar)

;;; eyebrowse-tab-bar.el ends here
