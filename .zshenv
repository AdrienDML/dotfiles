export VISUAL="nvim"
export EDITOR="nvim"
export TERMINAL="tilix"

export PATH="$HOME/bin:$HOME/.local/bin:$HOME/.cargo/bin:$HOME/.yarn/bin:$PATH"

#C++
export CC=clang
export CXX=clang++
export CTEST_OUTPUT_ON_FAILURE=1

#Qt
export QT_QPA_PLATFORMTHEME=qt5ct

#Java
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
export _JAVA_AWT_WM_NONREPARENTING=1

#International input
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

#FZF
export FZF_DEFAULT_OPTS="--preview 'if file -b {} | grep --quiet text; then bat --color=always --line-range :500 --style=numbers -- {}; elif file -b {} | grep --quiet directory; then ls -lah --color=always {}; else echo \`file -b {}\`; fi'"

#Make FZF ignore files that Ag (The Silver Searcher) ignores
#This includes stuff in .gitignore
export FZF_DEFAULT_COMMAND="ag -g ''"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

#Damn it Microsoft
export DOTNET_CLI_TELEMETRY_OPTOUT=1

#Rust
export RUST_BACKTRACE=full
export RUSTFLAGS="-C link-arg=-fuse-ld=lld" #LLD links faster than LD
