--- Rust layer
-- @module l.rust

local file = require("c.file")

local layer = {}

--- Returns plugins required for this layer
function layer.register_plugins()
end

--- Configures vim and plugins for this layer
function layer.init_config()
  local lsp = require("l.lsp")
  local build = require("l.build")
  local lspconfig = require("lspconfig")

  -- lsp.register_server(lspconfig.rls)
  lsp.register_server(lspconfig.rust_analyzer)

  -- Ignore cargo output
  file.add_to_wildignore("target")

  build.make_builder()
    :with_filetype("rust")
    :with_prerequisite_file("Cargo.toml")
    :with_build_command("cargo build")
    :with_test_command("cargo test")
    :add()
end

return layer
