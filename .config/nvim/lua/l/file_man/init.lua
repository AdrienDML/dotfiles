--- File management
-- @module l.file_man

local plug = require("c.plug")
local keybind = require("c.keybind")
local edit_mode = require("c.edit_mode")
local autocmd = require("c.autocmd")
local file = require("c.file")

local layer = {}

--- Returns plugins required for this layer
function layer.register_plugins()
  plug.add_plugin("preservim/nerdtree")
  plug.add_plugin("junegunn/fzf.vim")
end

local function new_file()
  local path = vim.fn.input(
    "New file: ",
    file.make_relative_to_home(file.parent(vim.api.nvim_buf_get_name(0))) .. file.path_sep,
    "file"
  )

  if path == "" then
    print("Cancelled")
    return
  end

  vim.cmd("edit " .. vim.fn.fnameescape(path))
end

--- Configures vim and plugins for this layer
function layer.init_config()
  keybind.bind_command(edit_mode.NORMAL, "<leader>0", ":NERDTreeFocus<CR>", { noremap = true }, "Focus file tree")
  keybind.bind_command(edit_mode.NORMAL, "<leader>pt", ":NERDTreeToggle<CR>", { noremap = true }, "Toggle file tree")

  -- <leader>b group name is set in l.editor
  keybind.set_group_name("<leader>p", "Projects")
  keybind.set_group_name("<leader>f", "Files")

  keybind.bind_command(edit_mode.NORMAL, "<leader>pf", ":Files<CR>", { noremap = true }, "Find file")
  keybind.bind_command(edit_mode.NORMAL, "<leader>bb", ":Buffers<CR>", { noremap = true }, "Find buffer")
  keybind.bind_command(edit_mode.NORMAL, "<leader>fr", ":History<CR>", { noremap = true }, "Recent files")
  keybind.bind_function(edit_mode.NORMAL, "<leader>fn", new_file, { noremap = true }, "New file")

  -- Show hidden files
  vim.api.nvim_set_var("NERDTreeShowHidden", 1)

  -- Indent guides shouldn't be enabled for fzf buffers
  if plug.has_plugin("vim-indent-guides") then
    -- We can't just do `table.insert(vim.g.indent_guides_exclude_filetypes, "fzf")` because of how nvim exposes vim
    -- variables to Lua
    local excluded = vim.g.indent_guides_exclude_filetypes
    table.insert(excluded, "fzf")
    vim.g.indent_guides_exclude_filetypes = excluded
  end

  autocmd.bind_filetype("fzf", function()
    vim.wo.number = false
    vim.wo.relativenumber = false
  end)
end

return layer
