--- Ruby layer
-- @module l.ruby

local autocmd = require("c.autocmd")
local file = require("c.file")

local layer = {}

local function on_filetype_ruby()
  vim.api.nvim_buf_set_option(0, "shiftwidth", 2)
  vim.api.nvim_buf_set_option(0, "tabstop", 2)
  vim.api.nvim_buf_set_option(0, "softtabstop", 2)
end

local function on_filetype_eruby()
  vim.b.AutoPairs = vim.fn.AutoPairsDefine{
    ["<%="] = "%>",
    ["<%#"] = "%>",
    ["<%"] = "%>",
  }
end

--- Returns plugins required for this layer
function layer.register_plugins()
end

--- Configures vim and plugins for this layer
function layer.init_config()
  local lsp = require("l.lsp")
  local lspconfig = require("lspconfig")

  lsp.register_server(lspconfig.solargraph)

  autocmd.bind_filetype("ruby", on_filetype_ruby)
  autocmd.bind_filetype("eruby", on_filetype_eruby)

  file.add_to_wildignore("vendor")
end

return layer
