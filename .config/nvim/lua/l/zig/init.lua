--- Zig layer
-- @module l.zig

local layer = {}

--- Registers plugins required for this layer
function layer.register_plugins()
end

--- Configures vim and plugins for this layer
function layer.init_config()
  local lsp = require("l.lsp")
  local util = require("lspconfig.util")
  local configs = require("lspconfig/configs")

  -- Pinched from https://github.com/neovim/nvim-lspconfig/pull/366
  configs.zls = {
    default_config = {
      cmd = {"zls"};
      filetypes = {"zig", "zir"};
      root_dir = function(fname)
        return util.root_pattern("zls.json", ".git")(fname) or util.path.dirname(fname)
      end
    };
    docs = {
      description = [[
           https://github.com/zigtools/zls
           `Zig LSP implementation + Zig Language Server`.
        ]];
      default_config = {
        root_dir = [[util.root_pattern("zls.json", ".git") or current_file_dirname]];
      };
    };
  }

  local lspconfig = require("lspconfig")
  lsp.register_server(lspconfig.zls)
end

return layer
