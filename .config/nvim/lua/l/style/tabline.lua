--- Custom tabline
-- @module l.style.tabline

local autocmd = require("c.autocmd")
local class = require("c.class")
local color = require("c.color")
local lsp = require("l.lsp")

local tabline = {}

local TabLineTextPart = class {
  content = class.NULL,
  width = class.NULL,
  highlight = class.NULL,

  __init = function(self, content, width, highlight)
    self.content = content
    self.width = width
    self.highlight = highlight or "CTabLine"
  end,
}

-- Deep purple
local TASK_BASE_COLOR = {
  103 / 255,
  58 / 255,
  183 / 255
}

-- Pink
local TASK_PROG_COLOR = {
  233 / 255,
  30 / 255,
  99 / 255
}

local tick_count = 0
local tick_timer = nil

function capitalize(str)
  return (str:gsub("^%l", string.upper))
end

local function tick_colors()
  local time = vim.fn.reltimefloat(vim.fn.reltime())

  -- Update breathing colors
  local adj = 0.15 * math.sin(time * 2)
  local task_hex = color.rgb_to_hex(
    TASK_BASE_COLOR[1] + adj,
    TASK_BASE_COLOR[2] + adj,
    TASK_BASE_COLOR[3] + adj
    )
  vim.cmd("hi CTabLineTask guifg=#FFFFFF guibg=#" .. task_hex)

  local task_bar_hex = color.rgb_to_hex(
    TASK_PROG_COLOR[1] + adj,
    TASK_PROG_COLOR[2] + adj,
    TASK_PROG_COLOR[3] + adj
    )
  vim.cmd("hi CTabLineTaskBar guifg=#FFFFFF guibg=#" .. task_bar_hex)
end

local function render()
  tick_count = tick_count + 1

  tick_colors()

  local left_sections = {}
  local right_sections = {}

  -- Add tabs
  for tab = 1, vim.fn.tabpagenr("$") do
    local win = vim.fn.tabpagewinnr(tab)
    local buf_list = vim.fn.tabpagebuflist(tab)
    local buf = buf_list[win]
    local buf_name = vim.fn.fnamemodify(vim.fn.bufname(buf), ":t")

    if buf_name == "" then
      buf_name = "[No name]"
    end

    local text = string.format("%%%dT %s ", tab, buf_name)
    local is_active = tab == vim.fn.tabpagenr()

    table.insert(left_sections, TabLineTextPart(
        text,
        2 + #buf_name,
        is_active and "CTabLineTabActive" or (tab % 2 == 0 and "CTabLineTab2" or "CTabLineTab1")
      ))
  end

  table.insert(left_sections, TabLineTextPart(
      "%X",
      0,
      "CTabLine"
    ))

  -- Add LSP progress reporting
  for _, v in pairs(lsp.client_progress) do
    local seg = {}
    if v.message ~= class.NULL then
      table.insert(seg, v.message)
    end
    table.insert(seg, capitalize(v.title))
    if v.percentage ~= class.NULL then
      local percent_text = string.format("%5.2f\001", v.percentage * 100)
      table.insert(seg, percent_text)
    end

    local text = " " .. table.concat(seg, " ") .. " "
    local display_len = #text
    if v.percentage ~= class.NULL then
      local prog_bar_char = math.floor(v.percentage * #text)
      text = "%#CTabLineTaskBar#" .. text:sub(1, prog_bar_char) .. "%#CTabLineTask#" .. text:sub(prog_bar_char + 1) .. "%#CTabLineTask#"
    end

    text = text:gsub("\001", "%%%%")

    local section = TabLineTextPart(
      text,
      display_len,
      "CTabLineTask"
      )
    table.insert(right_sections, section)
  end

  -- Get left sections
  local current_hl = "CTabLine"
  local left_strs = {"%#CTabLine#"}
  local left_len = 0
  for _, v in ipairs(left_sections) do
    if v.highlight ~= current_hl then
      table.insert(left_strs, "%#" .. v.highlight .. "#")
    end
    table.insert(left_strs, v.content)

    left_len = left_len + v.width
  end

  table.insert(left_strs, "%#CTabLine#")

  -- Get right sections
  current_hl = "CTabLine"
  local right_strs = {}
  local right_len = 0
  for _, v in ipairs(right_sections) do
    if v.highlight ~= current_hl then
      table.insert(right_strs, "%#" .. v.highlight .. "#")
    end
    table.insert(right_strs, v.content)

    right_len = right_len + v.width
  end

  local spacing = vim.o.columns - left_len - right_len

  local left = table.concat(left_strs, "")
  local right = table.concat(right_strs, "")
  if spacing < 0 then
    if #left + spacing < 1 then
      vim.o.tabline = left:sub(1, #left + spacing) .. right
    else
      vim.o.tabline = right
    end
  else
    vim.o.tabline = left .. string.rep(" ", spacing) .. right
  end
end

function tabline.init_config()
  -- Always show the tab line
  vim.o.showtabline = 2

  autocmd.bind_colorscheme(function()
    vim.cmd("hi CTabLine guifg=#FFFFFF guibg=#212121")
    vim.cmd("hi CTabLineTab1 guifg=#FFFFFF guibg=#303030")
    vim.cmd("hi CTabLineTab2 guifg=#FFFFFF guibg=#404040")
    vim.cmd("hi CTabLineTabActive guifg=#FFFFFF guibg=#E91E63")
    vim.cmd("hi CTabLineTask guifg=#FFFFFF guibg=#673AB7")
    vim.cmd("hi CTabLineTaskBar guifg=#FFFFFF guibg=#E91E63")
  end)

  -- lsp.signal_progress_update:connect(render)

  render()

  tick_timer = vim.loop.new_timer()
  tick_timer:start(1000 / 10, 1000 / 10, vim.schedule_wrap(render))
end

function tabline.__unload()
  if tick_timer ~= nil then
    tick_timer:close()
  end
end

return tabline
