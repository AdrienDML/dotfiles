--- Generate setters and getters
-- @module l.c_cpp.geddit

local keybind = require("c.keybind")
local edit_mode = require("c.edit_mode")

local geddit = {}

local function get_type_and_name(at_node)
  local ts_utils = require("nvim-treesitter.ts_utils")

  -- Search upwards for a "declaration"
  while at_node:type() ~= "declaration" do
    at_node = at_node:parent()

    if at_node == nil then
      return
    end
  end

  local type_name
  local line
  local var_name
  for child in at_node:iter_children() do
    local child_type = child:type()

    if child_type == "type_identifier" or child_type == "scoped_type_identifier" or child_type == "template_type" then
      type_name = table.concat(ts_utils.get_node_text(child), "\n")
      line, _, _ = child:start()
    elseif child_type == "identifier" then
      var_name = ts_utils.get_node_text(child)[1]
    elseif child_type == "init_declarator" then
      var_name = ts_utils.get_node_text(child:child(0))[1]
    elseif child_type == "pointer_declarator" then
      type_name = type_name .. "*"
      var_name = vim.trim(ts_utils.get_node_text(child)[1]:sub(2))
    end
  end

  return type_name, var_name, line
end

local function get_namespace(at_node)
  local ts_utils = require("nvim-treesitter.ts_utils")

  -- Search upwards for a "namespace definition"
  while at_node:type() ~= "namespace_definition" do
    at_node = at_node:parent()

    if at_node == nil then
      return
    end
  end

  -- The C++ treesitter parser doesn't handle C++17 nested namespaces yet
  return vim.trim(vim.split(vim.split(table.concat(ts_utils.get_node_text(at_node), " "), "namespace")[2], "{")[1])
end

local function get_class(at_node)
  local ts_utils = require("nvim-treesitter.ts_utils")

  -- Search upwards for a "function definition"...yes it thinks classes are functions
  while at_node:type() ~= "function_definition" do
    at_node = at_node:parent()

    if at_node == nil then
      return
    end
  end

  local before_brace = vim.split(table.concat(ts_utils.get_node_text(at_node), " "), "{")[1]
  local before_colon = vim.split(before_brace, ":")[1]
  local split_at_space = vim.split(vim.trim(before_colon), " ")

  return split_at_space[#split_at_space]
end

local function insert_at_public(text, at_node, indent)
  local fixed_text = {}
  local indent_str = string.rep(" ", indent)
  for _, v in ipairs(text) do
    table.insert(fixed_text, indent_str .. v)
  end

  -- Search upwards for a "compound statement" (the curley brackets of the class)
  while at_node:type() ~= "compound_statement" do
    at_node = at_node:parent()

    if at_node == nil then
      return
    end
  end

  -- Search for the last `public:` label
  local public_line = nil
  for child in at_node:iter_children() do
    if child:type() == "labeled_statement" then
      local start_row, start_col, _, end_col = child:range()
      local line = vim.api.nvim_buf_get_lines(0, start_row, start_row + 1, true)[1]:sub(start_col + 1, end_col)

      if line == "public:" then
        public_line = start_row
      end
    end
  end

  if public_line == nil then
    local last_class_line, _, _ = at_node:end_()
    vim.api.nvim_buf_set_lines(0, last_class_line, last_class_line, true, {"", string.rep(" ", indent - vim.bo.shiftwidth) .. "public:"})
    vim.api.nvim_buf_set_lines(0, last_class_line + 2, last_class_line + 2, true, fixed_text)
    return
  end

  -- Search for the next access modifier, or the end of the class
  for child in at_node:iter_children() do
    local line, _, _ = child:start()
    if line > public_line then
      if child:type() == "labeled_statement" then
        vim.api.nvim_buf_set_lines(0, line, line, true, fixed_text)
        -- vim.api.nvim_buf_set_lines(0, line + #fixed_text, line + #fixed_text, true, {""}) -- Add a blank line
        return
      end
    end
  end

  -- We didn't find any access modifiers after the public: one
  -- So insert at the end of the class
  local last_class_line, _, _ = at_node:end_()
  -- vim.api.nvim_buf_set_lines(0, last_class_line, last_class_line, true, {""})
  -- vim.api.nvim_buf_set_lines(0, last_class_line + 1, last_class_line + 1, true, fixed_text)
  vim.api.nvim_buf_set_lines(0, last_class_line, last_class_line, true, fixed_text)
end

local COPY_TYPES = {
  "char",
  "unsigned char",
  "signed char",
  "short",
  "unsigned short",
  "signed short",
  "int",
  "unsigned int",
  "signed int",
  "long",
  "unsigned long",
  "signed long",
  "long long",
  "unsigned long long",
  "signed long long",
  "float",
  "double",
  "long double",
  "bool",
  "BOOL", -- Sometimes found in C code
  "int8_t",
  "uint8_t",
  "int16_t",
  "uint16_t",
  "int32_t",
  "uint32_t",
  "int64_t",
  "uint64_t",
  "char16_t",
  "char32_t",
  "wchar_t",
  "size_t",
  "SIZE_T", -- Unreal Engine
  "int8",
  "uint8",
  "int16",
  "uint16",
  "int32",
  "uint32",
  "int64",
  "uint64",
  "qint8",
  "quint8",
  "qint16",
  "quint16",
  "qint32",
  "quint32",
  "qint64",
  "quint64",
  "i8",
  "s8",
  "u8",
  "i16",
  "s16",
  "u16",
  "i32",
  "s32",
  "u32",
  "i64",
  "s64",
  "u64",
  "%&$", -- Reference type
  "^shared_ptr<.*>$",
  "^TSharedPtr<.*>$", -- Unreal Engine
}

local CPP_EXTS = {
  ".cpp",
  ".cxx",
  ".cc",
  ".c++",
  ".C",
}

local function capitalize(str)
  return str:gsub("^%l", string.upper)
end

local function get_named_buf(name)
  local bufs = vim.api.nvim_list_bufs()

  for _, buf in ipairs(bufs) do
    if vim.endswith(vim.api.nvim_buf_get_name(buf), name) then
      return buf
    end
  end

  return nil
end

local function make_getter_setter(make_getter, make_const_getter, make_setter)
  local ts_utils = require("nvim-treesitter.ts_utils")
  local parsers = require("nvim-treesitter.parsers")

  local node = ts_utils.get_node_at_cursor()
  local type_name, var_name, line = get_type_and_name(node)

  if type_name == nil then
    print("Cursor not over a variable declaration!")
    return
  end

  local split = vim.split(type_name, "::")
  local type_name_no_ns = split[#split]

  local should_copy = false
  for _, v in ipairs(COPY_TYPES) do
    if type_name_no_ns:match(v) then
      should_copy = true
      break
    end
  end

  local capitalized = capitalize(var_name)

  local to_insert = {}
  if should_copy then
    if make_setter then table.insert(to_insert, "void set" .. capitalized .. "(" .. type_name .. " " .. var_name .. ");") end
    if make_const_getter then table.insert(to_insert, type_name .. " get" .. capitalized .. "() const;") end
  else
    if type_name_no_ns:match("%*$") then
      -- It's a pointer
      if make_setter then table.insert(to_insert, "void set" .. capitalized .. "(" .. type_name:sub(1, #type_name - 1) .. " *" .. var_name .. ");") end
      if make_getter then table.insert(to_insert, type_name .. " get" .. capitalized .. "();") end
      if make_const_getter then table.insert(to_insert, "const " .. type_name .. " get" .. capitalized .. "() const;") end
    else
      if make_setter then table.insert(to_insert, "void set" .. capitalized .. "(const " .. type_name .. " &" .. var_name .. ");") end
      if make_getter then table.insert(to_insert, type_name .. "& get" .. capitalized .. "();") end
      if make_const_getter then table.insert(to_insert, "const " .. type_name .. "& get" .. capitalized .. "() const;") end
    end
  end

  insert_at_public(to_insert, node, vim.fn.indent(line + 1))

  -- Try to find the corresponding .cpp file
  local cpp_buf
  for _, ext in ipairs(CPP_EXTS) do
    cpp_buf = get_named_buf(vim.fn.expand("%:t:r") .. ext)
    if cpp_buf ~= nil then break end
  end

  if cpp_buf == nil then
    print("Couldn't find matching .cpp file")
    return
  end

  -- Find the namespace our class is in, if any
  local class = get_class(node)
  local namespace = get_namespace(node)

  local cpp_to_insert = {}

  if should_copy then
    if make_setter then
      table.insert(cpp_to_insert, "")
      table.insert(cpp_to_insert, "void " .. class .. "::set" .. capitalized .. "(" .. type_name:sub(1, #type_name - 1) .. " &" .. var_name .. ") {")
      table.insert(cpp_to_insert, "    this->" .. var_name .. " = " .. var_name .. ";")
      table.insert(cpp_to_insert, "}")
    end

    if make_getter then
      table.insert(cpp_to_insert, "")
      table.insert(cpp_to_insert, type_name .. "& " .. class .. "::get" .. capitalized .. "() {")
      table.insert(cpp_to_insert, "    return " .. var_name .. ";")
      table.insert(cpp_to_insert, "}")
    end

    if make_const_getter then
      table.insert(cpp_to_insert, "")
      table.insert(cpp_to_insert, "const " .. type_name .. "& " .. class .. "::get" .. capitalized .. "() const {")
      table.insert(cpp_to_insert, "    return " .. var_name .. ";")
      table.insert(cpp_to_insert, "}")
    end
  else
    if type_name_no_ns:match("%*$") then
      if make_setter then
        table.insert(cpp_to_insert, "")
        table.insert(cpp_to_insert, "void " .. class .. "::set" .. capitalized .. "(" .. type_name:sub(1, #type_name - 1) .. " *" .. var_name .. ") {")
        table.insert(cpp_to_insert, "    this->" .. var_name .. " = " .. var_name .. ";")
        table.insert(cpp_to_insert, "}")
      end

      if make_getter then
        table.insert(cpp_to_insert, "")
        table.insert(cpp_to_insert, type_name .. " " .. class .. "::get" .. capitalized .. "() {")
        table.insert(cpp_to_insert, "    return " .. var_name .. ";")
        table.insert(cpp_to_insert, "}")
      end

      if make_const_getter then
        table.insert(cpp_to_insert, "")
        table.insert(cpp_to_insert, "const " .. type_name .. " " .. class .. "::get" .. capitalized .. "() const {")
        table.insert(cpp_to_insert, "    return " .. var_name .. ";")
        table.insert(cpp_to_insert, "}")
      end
    else
      if make_setter then
        table.insert(cpp_to_insert, "")
        table.insert(cpp_to_insert, "void " .. class .. "::set" .. capitalized .. "(const " .. type_name .. " &" .. var_name .. ") {")
        table.insert(cpp_to_insert, "    this->" .. var_name .. " = " .. var_name .. ";")
        table.insert(cpp_to_insert, "}")
      end

      if make_getter then
        table.insert(cpp_to_insert, "")
        table.insert(cpp_to_insert, type_name .. "& " .. class .. "::get" .. capitalized .. "() {")
        table.insert(cpp_to_insert, "    return " .. var_name .. ";")
        table.insert(cpp_to_insert, "}")
      end

      if make_const_getter then
        table.insert(cpp_to_insert, "")
        table.insert(cpp_to_insert, "const " .. type_name .. "& " .. class .. "::get" .. capitalized .. "() const {")
        table.insert(cpp_to_insert, "    return " .. var_name .. ";")
        table.insert(cpp_to_insert, "}")
      end
    end
  end

  if namespace ~= nil then
    local root = parsers.get_parser(cpp_buf):parse():root()
    for cpp_node in root:iter_children() do
      if cpp_node:type() == "namespace_definition" then
        local namespace_name = vim.trim(vim.split(vim.split(table.concat(ts_utils.get_node_text(cpp_node, cpp_buf), " "), "namespace")[2], "{")[1])
        if namespace_name == namespace then
          local ns_end, _, _ = cpp_node:end_()

          -- Indent everything
          local indent_str = string.rep(" ", vim.api.nvim_buf_get_option(cpp_buf, "shiftwidth"))
          for k, v in ipairs(cpp_to_insert) do
            if v ~= "" then
              cpp_to_insert[k] = indent_str .. v
            end
          end

          vim.api.nvim_buf_set_lines(cpp_buf, ns_end, ns_end, true, cpp_to_insert)
          return
        end
      end
    end

    -- We gotta make the namespace outselves

    -- First, indent everything
    local indent_str = string.rep(" ", vim.api.nvim_buf_get_option(cpp_buf, "shiftwidth"))
    for k, v in ipairs(cpp_to_insert) do
      if v ~= "" then
        cpp_to_insert[k] = indent_str .. v
      end
    end

    -- Now surround with a namespace
    table.insert(cpp_to_insert, 2, "namespace " .. namespace .. " {") -- Index 1 is an empty line
    table.insert(cpp_to_insert, "}")

    vim.api.nvim_buf_set_lines(cpp_buf, -1, -1, true, cpp_to_insert)
  else
    vim.api.nvim_buf_set_lines(cpp_buf, -1, -1, true, cpp_to_insert)
  end
end

function geddit.init()
  -- TODO: Only in cpp buffers
  keybind.bind_function(edit_mode.NORMAL, "<localleader>gs", function() make_getter_setter(false, true, true) end, { noremap = true }, "Generate getter/setter const")
  keybind.bind_function(edit_mode.NORMAL, "<localleader>gS", function() make_getter_setter(true, true, true) end, { noremap = true }, "Generate getter/setter const + mut")
  keybind.bind_function(edit_mode.NORMAL, "<localleader>gg", function() make_getter_setter(false, true, false) end, { noremap = true }, "Generate setter")
end

return geddit
