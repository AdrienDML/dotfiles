--- Cog layer
-- @module l.cog

local command = require("c.command")
local autocmd = require("c.autocmd")
local log = require("c.log")

local layer = {}

local function run_cog(args, opts)
  if vim.bo.modified then
    log.set_highlight("WarningMsg")
    log.log("Buffer has unsaved changes!")
    log.set_highlight("None")
  else
    log.log(vim.fn.trim(vim.fn.system("cog -rc " .. vim.fn.shellescape(vim.fn.expand("%")))))

    if vim.v.shell_error == 0 then
      vim.cmd("edit") -- Reload the file
    end
  end
end

local function on_any_filetype()
  old_syntax = vim.b.current_syntax

  -- Need this check for nvim to not error about this key not being found
  if vim.b.current_syntax ~= nil then
    vim.b.current_syntax = nil
  end

  vim.cmd("syntax include @EmbeddedPython syntax/python.vim")
  -- vim.cmd("syntax region cogPython matchgroup=CogPython start=\"\\[\\[\\[cog\" end=\"\\]\\]\\]\" contains=@EmbeddedPython containedin=.*Comment,vimCommentTitle,cCommentL")
  vim.cmd("syntax region cogPython start=\"\\[\\[\\[cog\" end=\"\\]\\]\\]\" contains=@EmbeddedPython containedin=.*Comment,vimCommentTitle,cCommentL")

  vim.b.current_syntax = old_syntax
end

--- Registers plugins required for this layer
function layer.register_plugins()
end

--- Configures vim and plugins for this layer
function layer.init_config()
  -- Add Cog command to run cog
  command.make_command("Cog", run_cog, 0)

  -- Add Python syntax highlighting for cog regions
  autocmd.bind_filetype("*", on_any_filetype)
end

return layer
