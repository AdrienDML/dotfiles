--- Python layer
-- @module l.python

local layer = {}

--- Returns plugins required for this layer
function layer.register_plugins()
end

--- Configures vim and plugins for this layer
function layer.init_config()
  local lsp = require("l.lsp")
  local lspconfig = require("lspconfig")

  lsp.register_server(lspconfig.pylsp)
end

return layer
