--- Color helpers
-- @module c.color

local mathext = require("c.mathext")

local color = {}

function color.rgb_to_hex(r, g, b)
  return string.format(
    "%02X%02X%02X",
    mathext.clamp(r * 255, 0, 255),
    mathext.clamp(g * 255, 0, 255),
    mathext.clamp(b * 255, 0, 255)
    )
end

return color
