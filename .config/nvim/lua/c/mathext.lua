--- Extra math stuff
-- @module c.mathext

local mathext = {}

function mathext.clamp(val, min, max)
  return math.max(math.min(val, max), min)
end

return mathext
