local awful = require("awful")

local media = {}

function media.prev()
  awful.spawn("playerctl previous", false)
  require("widgets.amedia").show_popup()
end

function media.next()
  awful.spawn("playerctl next", false)
  require("widgets.amedia").show_popup()
end

function media.toggle_play()
  awful.spawn("playerctl play-pause", false)
  require("widgets.amedia").show_popup()
end

function media.get_title_async(callback)
  awful.spawn.easy_async("playerctl metadata xesam:title", function(stdout, stderr, reason, exit_code)
    callback(stdout)
  end)
end

function media.get_artist_async(callback)
  awful.spawn.easy_async("playerctl metadata xesam:artist", function(stdout, stderr, reason, exit_code)
    callback(stdout)
  end)
end

return media
