local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local wibox = require("wibox")
local naughty = require("naughty")

local aswitch = require("widgets.aswitch")

-- local tableutils = require("tableutils")
-- local stringutils = require("stringutils")
local easing = require("easing")
local anim = require("anim")
local statics = require("statics")

local anotifarea = {}

anotifarea.sidebar_contents = wibox.layout.fixed.vertical()

local dnd_switch = wibox.widget{
  checked = naughty.is_suspended(),
  forced_height = 18,
  widget = aswitch,
}

dnd_switch:connect_signal("property::checked", function(switch)
  if switch:get_checked() then
    naughty.suspend()
  else
    naughty.resume()
  end
end)

local HIDE_ICON_PATH = gears.filesystem.get_xdg_config_home() ..
  "/awesome/images/material-design-icons/navigation/2x_web/ic_close_white_48dp.png"

local hide_button = wibox.widget{
  image = HIDE_ICON_PATH,
  forced_height = statics.font_size_title,
  widget = wibox.widget.imagebox,
}

hide_button:connect_signal("button::press", function(_, _, _, button)
  if button == 1 then
    anotifarea.hide()
  end
end)

------------------------------------------------------------------------------------------------------------------------

local cpu_graph = wibox.widget{
  max_value = 100,
  background_color = "#00000000",
  step_width = 1,
  width = 512 - 20,
  step_spacing = 0,
  forced_height = 100,
  color = beautiful.bg_focus,
  widget = wibox.widget.graph,
}

local cpu_text = wibox.widget{
  text = "0%",
  font = statics.font_subtitle,
  widget = wibox.widget.textbox,
}

local cpu_container = wibox.container.mirror(cpu_graph, { horizontal = true })

local total_prev = 0
local idle_prev = 0

awful.widget.watch([[bash -c "cat /proc/stat | grep '^cpu '"]], 1,
  function(widget, stdout)
    local user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice =
      stdout:match('(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s')

    local total = user + nice + system + idle + iowait + irq + softirq + steal

    local diff_idle = idle - idle_prev
    local diff_total = total - total_prev
    local diff_usage = (1000 * (diff_total - diff_idle) / diff_total + 5) / 10

    cpu_text.text = string.format("%.0f%%", diff_usage)

    widget:add_value(diff_usage)

    total_prev = total
    idle_prev = idle
  end,
  cpu_graph
)

------------------------------------------------------------------------------------------------------------------------

local mem_graph = wibox.widget{
  max_value = 100,
  background_color = "#00000000",
  step_width = 1,
  width = 512 - 20,
  step_spacing = 0,
  forced_height = 100,
  stack = true,
  stack_colors = { beautiful.bg_focus, beautiful.bg_focus .. "55" },
  widget = wibox.widget.graph,
}

local swap_graph = wibox.widget{
  max_value = 100,
  background_color = "#00000000",
  step_width = 1,
  width = 512 - 20,
  step_spacing = 0,
  forced_height = 100,
  color = beautiful.bg_minimize .. "cc",
  widget = wibox.widget.graph,
}

local mem_container = wibox.widget{
  wibox.container.mirror(mem_graph, { horizontal = true }),
  wibox.container.mirror(swap_graph, { horizontal = true }),
  layout = wibox.layout.stack,
}

local mem_text = wibox.widget{
  text = "0.00 / 0.00 GiB<span size=\"smaller\"> + 0.00 GiB swap</span>",
  font = statics.font_subtitle,
  widget = wibox.widget.textbox,
}

local function iter_lines(s)
  if s:sub(-1)~="\n" then s=s.."\n" end
  return s:gmatch("(.-)\n")
end

awful.widget.watch('cat /proc/meminfo', 1,
  function(widget, stdout, stderr, exitreason, exitcode)
    -- local total, used, free, shared, buff_cache, available, total_swap, used_swap, free_swap =
      -- stdout:match('(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*Swap:%s*(%d+)%s*(%d+)%s*(%d+)')

    local total = 0
    local active = 0
    local inactive = 0
    local swap_total = 0
    local swap_free = 0
    for line in iter_lines(stdout) do
      local total_match = line:match("MemTotal:%s+([0-9]+)")
      if total_match ~= nil then
        total = tonumber(total_match) * 1024
      else
        local active_match = line:match("Active:%s+([0-9]+)")
        if active_match ~= nil then
          active = tonumber(active_match) * 1024
        else
          local inactive_match = line:match("Inactive:%s+([0-9]+)")
          if inactive_match ~= nil then
            inactive = tonumber(inactive_match) * 1024
          else
            local swap_total_match = line:match("SwapTotal:%s+([0-9]+)")
            if swap_total_match ~= nil then
              swap_total = tonumber(swap_total_match) * 1024
            else
              local swap_free_match = line:match("SwapFree:%s+([0-9]+)")
              if swap_free_match ~= nil then
                swap_free = tonumber(swap_free_match) * 1024
              end
            end
          end
        end
      end
    end

    local swap_used = swap_total - swap_free

    mem_text.markup = string.format(
      "%.2f<span size=\"smaller\"> / %.2f GiB + %.2f GiB inact + %.2f GiB swp</span>",
      active / (1024 ^ 3),
      total / (1024 ^ 3),
      inactive / (1024 ^ 3),
      swap_used / (1024 ^ 3)
    )

    widget.max_value = total
    widget:add_value(active, 1)
    widget:add_value(inactive, 2)

    swap_graph.max_value = swap_total
    swap_graph:add_value(swap_used, 2)
  end,
  mem_graph
)

------------------------------------------------------------------------------------------------------------------------

anotifarea.sidebar_wibox = wibox{
  widget = wibox.widget{
    {
      {
        {
          text = "Dashboard",
          font = statics.font_title,
          widget = wibox.widget.textbox,
        },
        nil,
        hide_button,
        layout = wibox.layout.align.horizontal,
      },
      {
        {
          text = "CPU usage",
          font = statics.font_subtitle,
          widget = wibox.widget.textbox,
        },
        nil,
        cpu_text,
        layout = wibox.layout.align.horizontal,
      },
      cpu_container,
      {
        {
          text = "Memory usage",
          font = statics.font_subtitle,
          widget = wibox.widget.textbox,
        },
        nil,
        mem_text,
        layout = wibox.layout.align.horizontal,
      },
      mem_container,
      {
        text = "Notifications",
        font = statics.font_subtitle,
        widget = wibox.widget.textbox,
      },
      {
        {
          text = "Do not disturb",
          widget = wibox.widget.textbox,
        },
        nil,
        dnd_switch,
        layout = wibox.layout.align.horizontal,
      },
      anotifarea.sidebar_contents,
      spacing = 4,
      layout = wibox.layout.fixed.vertical,
    },
    margins = 10,
    widget = wibox.container.margin,
  },
  ontop = true,
  visible = false,
  width = 512,
}

function anotifarea.show()
  anotifarea.sidebar_wibox.visible = true
  anotifarea.sidebar_wibox.height = awful.screen.focused().geometry.height

  dnd_switch:set_checked(naughty.is_suspended())

  anim.animate{
    start_val = -96,
    end_val = 0,
    prop_table = anotifarea.sidebar_wibox,
    prop_name = "x",
    duration = 0.5,
    easing = easing.outExpo,
  }
  anim.animate{
    start_val = 0,
    end_val = 0.95,
    prop_table = anotifarea.sidebar_wibox,
    prop_name = "opacity",
    duration = 0.5,
    easing = easing.outExpo,
  }
end

function anotifarea.hide()
  anotifarea.sidebar_wibox.visible = false
end

function anotifarea.toggle_visibility()
  if anotifarea.sidebar_wibox.visible then
    anotifarea.hide()
  else
    anotifarea.show()
  end
end

function anotifarea.make_notif_widget(parent, args)
  local spacer = wibox.widget{
    widget = wibox.widget.textbox,
  }

  local image_widget
  local image_spacer
  if args.icon then
    image_widget = wibox.widget{
      image = args.icon,
      resize = true,
      forced_height = 40,
      forced_width = 40,
      widget = wibox.widget.imagebox,
    }

    image_spacer = wibox.widget{
      spacer,
      forced_width = 4,
      widget = wibox.container.background
    }
  end

  local RUN_ICON_PATH = gears.filesystem.get_xdg_config_home() ..
    "/awesome/images/material-design-icons/navigation/2x_web/ic_chevron_right_white_48dp.png"

  local DISMISS_ICON_PATH = gears.filesystem.get_xdg_config_home() ..
    "/awesome/images/material-design-icons/navigation/2x_web/ic_close_white_48dp.png"

  local run_button
  if args.run then
    run_button = wibox.widget{
      image = RUN_ICON_PATH,
      forced_height = 20,
      forced_width = 20,
      widget = wibox.widget.imagebox,
    }

    run_button:connect_signal("button::press", function(_, _, _, button)
      if button == 1 then
        args.run()
      end
    end)
  end

  local dismiss_button = wibox.widget{
    image = DISMISS_ICON_PATH,
    forced_height = 20,
    forced_width = 20,
    widget = wibox.widget.imagebox,
  }

  local widget = wibox.widget{
    {
      {
        {
          spacer,
          bg = beautiful.bg_focus,
          forced_width = 4,
          widget = wibox.container.background
        },
        {
          spacer,
          forced_width = 4,
          widget = wibox.container.background
        },
        image_widget,
        image_spacer,
        {
          {
            {
              text = gears.string.xml_unescape(args.title), -- Title may be optional
              font = statics.font_bold,
              wrap = "word_char",
              widget = wibox.widget.textbox,
            },
            {
              text = gears.string.xml_unescape(args.text),
              wrap = "word_char",
              widget = wibox.widget.textbox,
            },

            layout = wibox.layout.fixed.vertical,
          },

          top = 4,
          bottom = 4,
          widget = wibox.container.margin,
        },
        layout = wibox.layout.fixed.horizontal,
      },

      nil,
      {
        dismiss_button,
        run_button,

        layout = wibox.layout.fixed.vertical,
      },

      layout = wibox.layout.align.horizontal,
    },

    top = 4,
    bottom = 4,
    widget = wibox.container.margin,
  }

  dismiss_button:connect_signal("button::press", function(_, _, _, button)
    if button == 1 then
      -- parent:remove_widgets(widget, true)
      anim.animate{
        start_val = widget.children[1].height + 8,
        end_val = 20,
        prop_table = widget,
        prop_name = "forced_height",
        duration = 0.5,
        easing = easing.outExpo,
      }
    end
  end)

  return widget
end

function anotifarea.on_notification(args)
  -- args.icon_data = nil
  -- args.freedesktop_hints.icon_data = nil
  -- print(tableutils.to_string(args))

  -- anotifarea.sidebar_contents:add(anotifarea.make_notif_widget(anotifarea.sidebar_contents, args))

  return args
end

-- naughty.config.notify_callback = anotifarea.on_notification

return anotifarea
