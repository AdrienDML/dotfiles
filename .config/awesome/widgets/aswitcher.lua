local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")

local tableutils = require("tableutils")
local statics = require("statics")

local aclientcontent = require("widgets.aclientcontent")

local aswitcher = {}

local vbox = wibox.widget{
  spacing = 4,
  widget = wibox.layout.fixed.vertical,
}

local no_clients_text = wibox.widget{
  text = "No clients",
  font = statics.font_title,
  widget = wibox.widget.textbox,
  visible = false,
}

aswitcher.switcher_wibox = awful.popup{
  widget = {
    {
      vbox,
      no_clients_text,

      widget = wibox.layout.fixed.vertical,
    },

    widget = wibox.container.margin,
    margins = 4,
  },
  border_color = beautiful.bg_focus,
  border_width = 2,
  opacity = 0.98,
  ontop = true,
  visible = false,
  placement = awful.placement.centered,
}

function aswitcher.show()
  aswitcher.switcher_wibox.visible = true
  awful.client.focus.history.disable_tracking()
end

function aswitcher.hide()
  aswitcher.switcher_wibox.visible = false
  awful.client.focus.history.enable_tracking()
  if client.focus ~= nil then
    awful.client.focus.history.add(client.focus)
  end
end

function aswitcher.toggle_visibility()
  if aswitcher.switcher_wibox.visible then
    aswitcher.hide()
  else
    aswitcher.show()
  end
end

local client_list = nil
local idx = 0

local function update_ui_list()
  local children = tableutils.shallow_copy(vbox.children)
  for _, v in pairs(children) do
    vbox:remove_widgets(v)
  end

  for k, v in ipairs(client_list) do
    local client_content = wibox.widget{
      widget = aclientcontent,
      forced_width = 85,
      forced_height = 48,
    }

    local widget = wibox.widget{
      {
        {
          id = "client_icon",
          client = v.client,
          widget = awful.widget.clienticon,
          forced_width = 48,
          forced_height = 48,
        },
        client_content,
        {
          id = "client_title",
          widget = wibox.widget.textbox,
          font = statics.font,
          text = v.client.name,
        },
        spacing = 8,
        widget = wibox.layout.fixed.horizontal,
      },
      id = "background_role",
      bg = idx + 1 == k and beautiful.bg_focus or nil,
      widget = wibox.container.background,
      forced_width = 512,
      forced_height = 48,
    }

    client_content:set_client(v.client)

    vbox:add(widget)
  end

  no_clients_text.visible = #client_list == 0
end

function aswitcher.on_tab(direction)
  if #client_list == 0 then return end

  local prev_client = client_list[idx + 1]
  if prev_client.was_minimized then
    prev_client.client.minimized = true
  end

  idx = idx + direction
  idx = idx % #client_list

  client_list[idx + 1].client:activate{
    switch_to_tag = true,
  }

  update_ui_list()
end

local function start(filter)
  local active_client = client.focus
  local active_screen

  if active_client ~= nil then
    active_screen = active_client.screen
  else
    active_screen = mouse.screen
  end

  client_list = {}
  for _, v in ipairs(awful.client.focus.history.list) do
    if filter(v, active_screen) then
      table.insert(client_list, { client = v, was_minimized = v.minimized })
    end
  end

  if client.focus == nil then
    idx = -1
  else
    idx = 0
  end

  update_ui_list()

  aswitcher.show()
end

awful.keygrabber{
  keybindings = {
    {{"Mod1", "Shift"}, "Tab", function() aswitcher.on_tab(-1) end},
    {{"Mod1"         }, "Tab", function() aswitcher.on_tab(1) end},
  },
  -- Note that it is using the key name and not the modifier name.
  stop_key           = "Mod1",
  stop_event         = "release",
  start_callback     = function() start(awful.widget.tasklist.filter.alltags) end,
  stop_callback      = aswitcher.hide,
  export_keybindings = true,
}

awful.keygrabber{
  keybindings = {
    {{"Mod4", "Shift"}, "Tab", function() aswitcher.on_tab(-1) end},
    {{"Mod4"         }, "Tab", function() aswitcher.on_tab(1) end},
  },
  -- Note that it is using the key name and not the modifier name.
  stop_key           = "Mod4",
  stop_event         = "release",
  start_callback     = function() start(awful.widget.tasklist.filter.currenttags) end,
  stop_callback      = aswitcher.hide,
  export_keybindings = true,
}

return aswitcher
