local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local wibox = require("wibox")
local naughty = require("naughty")

local easing = require("easing")
local anim = require("anim")
local statics = require("statics")
local command_registry = require("command_registry")

local awidgeteditor = {}

local HIDE_ICON_PATH = gears.filesystem.get_xdg_config_home() ..
  "/awesome/images/material-design-icons/navigation/2x_web/ic_close_white_48dp.png"

local hide_button = wibox.widget{
  image = HIDE_ICON_PATH,
  forced_height = statics.font_size_title,
  widget = wibox.widget.imagebox,
}

hide_button:connect_signal("button::press", function(_, _, _, button)
  if button == 1 then
    awidgeteditor.hide()
  end
end)

awidgeteditor.window_wibox = wibox{
  widget = wibox.widget{
    {
      {
        {
          text = "Widget Editor",
          font = statics.font_title,
          widget = wibox.widget.textbox,
        },
        nil,
        hide_button,

        layout = wibox.layout.align.horizontal,
      },

      spacing = 4,
      layout = wibox.layout.fixed.vertical,
    },

    margins = 10,
    widget = wibox.container.margin,
  },

  x = 32,
  y = 32,
  ontop = true,
  visible = false,
}

function awidgeteditor.init()
  command_registry.add_command{
    name = "widget-editor",
    action = awidgeteditor.show,
  }
end

function awidgeteditor.show()
  awidgeteditor.window_wibox.visible = true
  awidgeteditor.window_wibox.height = awful.screen.focused().geometry.height - 64
  awidgeteditor.window_wibox.width = awful.screen.focused().geometry.width - 64
end

function awidgeteditor.hide()
  awidgeteditor.window_wibox.visible = false
end

return awidgeteditor
