local gears = require("gears")
local beautiful = require("beautiful")
local base = require("wibox.widget.base")
local awful = require("awful")

local switch = {}

local function fit(_, _, w, h)
  local height = math.min(w / 2, h)
  return height * 2, height
end

local function draw(self, _, cr, w, h)
  local height = math.min(w / 2, h)
  local width = height * 2

  local half_height = height / 2

  local handle_x = self._private.checked and width - half_height or half_height
  local bg_circle_x = self._private.checked and half_height or width - half_height
  local bg_circle_rot = self._private.checked and 0.5 or 1.5
  local fg_color = self._private.checked and self:get_color_on() or self:get_color_off()
  local bg_color = self._private.checked and self:get_bg_on() or self:get_bg_off()

  cr:set_source(gears.color(bg_color))

  -- Draw background circle
  cr:arc(
    bg_circle_x, -- X
    height / 2, -- Y
    height / 3, -- Radius
    math.pi * bg_circle_rot, -- Arc begin
    math.pi * (bg_circle_rot + 1) -- Arc end
  )

  -- Draw background rectangle
  cr:rectangle(
    half_height,
    height / 6,
    width / 2,
    height / 1.5
  )

  cr:fill()
  cr:set_source(gears.color(fg_color))

  -- Draw handle
  cr:arc(
    handle_x, -- X
    half_height, -- Y
    half_height, -- Radius
    0, -- Arc begin
    2 * math.pi -- Arc end
  )

  cr:fill()
end

for _, prop in ipairs{"bg_off", "bg_on", "color_off", "color_on", "checked"} do
  switch["set_" .. prop] = function(self, value)
    self._private[prop] = value
    self:emit_signal("property::" .. prop)
    self:emit_signal("widget::redraw_needed")
  end

  switch["get_" .. prop] = function(self)
    return self._private[prop] or beautiful["switch_" .. prop]
  end
end

function switch:toggle()
  self:set_checked(not self._private.checked)
end

local function new(args)
  args = args or {}

  local ret = base.make_widget(nil, nil, {enable_properties = true})

  gears.table.crush(ret, switch)

  ret._private.checked = args.checked or false

  local buttons = gears.table.join(
    awful.button({}, 1, function() ret:toggle() end)
  )

  ret:buttons(buttons)

  rawset(ret, "fit" , fit)
  rawset(ret, "draw", draw)

  return ret
end

return setmetatable({}, {__call = function(_, ...) return new(...) end})
