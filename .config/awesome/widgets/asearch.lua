local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local menubar = require("menubar")

local easing = require("easing")

local anim = require("anim")
local statics = require("statics")

local asearch = {}

asearch.app_list = {}

local SEARCH_ICON_PATH = gears.filesystem.get_xdg_config_home() ..
  "/awesome/images/material-design-icons/action/2x_web/ic_search_white_48dp.png"

local APP_ICON_PATH = gears.filesystem.get_xdg_config_home() ..
  "/awesome/images/material-design-icons/av/2x_web/ic_web_asset_white_48dp.png"

local search_icon_32 = {
  id = "icon",
  image = SEARCH_ICON_PATH,
  resize = true,
  forced_width = 32,
  forced_height = 32,
  widget = wibox.widget.imagebox,
}

function asearch.create_app_list_item(descriptor)
  return wibox.widget{
    {
      image = descriptor.icon or APP_ICON_PATH,
      resize = true,
      forced_width = 32,
      forced_height = 32,
      widget = wibox.widget.imagebox,
    },
    {
      font = statics.font_name .. " 10",
      text = descriptor.name,
      widget = wibox.widget.textbox,
    },
    spacing = 16,
    widget = wibox.layout.fixed.horizontal,
  }
end

local results_container = wibox.widget{
  -- forced_height = 32 * 20 + 4 * 19, -- TEMP
  spacing = 4,
  widget = wibox.layout.fixed.vertical,
}

local files_results = wibox.widget{
  text = "",
  widget = wibox.widget.textbox,
}

local locate_pid = nil

local selected_index = 1

asearch.prompt = awful.widget.prompt{
  prompt = "",
  font = statics.font_name .. " 18",
  hook = {
    {{}, "Up", function(_) selected_index = selected_index - 1 end},
    {{}, "Down", function(_) selected_index = selected_index + 1 end},
  },
  done_callback = function()
    asearch.hide()
  end,
  exe_callback = function(query)
    -- TODO
  end,
  changed_callback = function(query)
    local run = function()
      files_results.text = ""
      locate_pid = awful.spawn.with_line_callback(
        {"locate", "-l", "20", "-i", query},
        {
          stdout = function(line)
            files_results.text = files_results.text .. line .. "\n"
          end,
          exit = function(_, _)
            locate_pid = nil
          end,
        }
      )
    end

    if locate_pid then
      awful.spawn(
        {"kill", tostring(locate_pid)},
        false,
        function()
          run()
        end
      )
    else
      run()
    end

  end,
  highlighter = function(b, a)
    local query = b .. a

    results_container:reset()
    local found = {}
    local i = 1
    for _, v in pairs(asearch.app_list) do
      if v.name:lower():find(query:lower()) then
        results_container:add(asearch.create_app_list_item(v))
        table.insert(found, v)

        i = i + 1
        if i > 20 then break end
      end
    end

    if query ~= "" and found[selected_index] then
      local index = found[selected_index].name:lower():find(query:lower())

      return "<span foreground='" .. beautiful.fg_normal .. "55'>" ..
        found[selected_index].name:sub(1, index - 1) ..
        "</span>" ..
        b,
      a ..
        "<span foreground='" .. beautiful.fg_normal .. "55'>" ..
        found[selected_index].name:sub(index + #query) ..
        "</span>"
    else
      return b, a
    end
  end,
}

local popup = awful.popup{
  widget = {
    {
      {
        search_icon_32,
        asearch.prompt,
        layout = wibox.layout.fixed.horizontal,
        spacing = 16,
        forced_width = 800,
      },
      results_container,
      files_results, -- TEMP
      widget = wibox.layout.fixed.vertical,
    },
    margins = 10,
    widget = wibox.container.margin,
  },
  ontop = true,
  y = 32,
  opacity = 0.95,
  visible = false,
}

function asearch.hide()
  popup.visible = false
end

function asearch.show()
  popup.visible = true
  asearch.prompt:run()

  -- anim.animate{
  --   start_val = -64,
  --   end_val = 32,
  --   prop_table = popup,
  --   prop_name = "y",
  --   duration = 0.5,
  --   easing = easing.outExpo,
  -- }
  -- anim.animate{
  --   start_val = 0,
  --   end_val = 0.95,
  --   prop_table = popup,
  --   prop_name = "opacity",
  --   duration = 0.5,
  --   easing = easing.outExpo,
  -- }
  popup.x = popup.screen.geometry.width / 2 - popup.width / 2 -- Centered on the screen
end

function asearch.toggle_visibility()
  if popup.visible then
    asearch.hide()
  else
    asearch.show()
  end
end

function asearch.init()
  print("Initializing asearch")

  menubar.menu_gen.generate(function(entries)
      print("Finished finding app entries in asearch")
      asearch.app_list = entries
  end)
end

return asearch
