local wibox = require("wibox")
local surface = require("gears.surface")
local shape = require("gears.shape")
local gears = require("gears")
local beautiful = require("beautiful")

local module = {}

local function fit(self, context, width, height)
  local size = math.min(width, height)
  return size, size
end

local function set_client(self, c)
  self._private.client[1] = c
  self:emit_signal("widget::redraw_needed")
end

local function draw(self, content, cr, width, height)
  local c = self._private.client[1]

  local geo = c:geometry()
  local scale = math.min(width / geo.width, height / geo.height)
  local w, h = geo.width * scale, geo.height * scale
  local dx, dy = (width - w) / 2, (height - h) / 2
  cr:translate(dx, dy)

  cr:set_source(gears.color(beautiful.bg_minimize))

  shape.rectangle(cr, w, h)
  cr:clip()
  cr:scale(scale, scale)

  if c:isvisible() then
    local s = surface(c.content)
    cr:set_source_surface(s)
  else
  end

  cr:paint()

  gears.timer{
    timeout = 0.5,
    autostart = true,
    callback = function() self:emit_signal("widget::redraw_needed") end,
    single_shot = true,
  }
end

local function new(c)
  local ret = wibox.widget.base.make_widget(nil, nil, {
      enable_properties = true,
    })

  rawset(ret, "fit", fit)
  rawset(ret, "draw", draw)
  rawset(ret, "set_client", set_client)
  ret._private.client = setmetatable({c}, {__mode="v"})
  return ret
end

return setmetatable(module, {__call=function(_,...) return new(...) end})
