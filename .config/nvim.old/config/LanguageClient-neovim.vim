let g:LanguageClient_serverCommands = {
            \ 'cpp': ['cquery', '--log-file=/tmp/cquery.log'],
            \ }
let g:LanguageClient_loadSettings = 1
let g:LanguageClient_settingsPath = '/home/mike/.config/nvim/settings.json'

