DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

declare -a IMAGES=(
    "https://vignette1.wikia.nocookie.net/glee/images/b/bf/-Mahou-Shoujo-Madoka-Magica-Kaname-Madoka-Anime-Akemi-Homura-Simple-Background-Anime-Girls-Fresh-New-Hd-Wallpaper--.jpg/revision/latest?cb=20131221162943"
    "https://static.zerochan.net/Akuma.Homura.full.1868960.jpg"
    "https://i.pinimg.com/originals/3b/5a/1a/3b5a1a5ea086ec3eb245fba4303bc884.jpg"
    "https://i.imgur.com/Cwuwtam.jpg" #Homura against cityscape - blue - Originally from https://i.imgur.com/mTxRegC.jpg
    "https://orig15.deviantart.net/a195/f/2011/145/d/f/homura_wallpaper_revisited_by_ch1zuruu-d3h6znz.jpg"
    "https://images2.alphacoders.com/110/thumb-1920-110030.jpg"
    "https://images7.alphacoders.com/332/332329.jpg"
    "https://i.imgur.com/mTxRegC.jpg" #Homura against cityscape - dark - Originally from http://www.wallpaperup.com/wallpaper/download/55301
    "http://i.imgur.com/N9sq9.jpg"
    "https://i.imgur.com/I9GBvEn.png" #Doki Doki Yuri
    "https://i.imgur.com/yrgTSYt.png" #"Never leave you alone" Originally from https://www.pixiv.net/member_illust.php?mode=big&illust_id=43604401
    "https://static.zerochan.net/No.Game.No.Life.full.1731140.jpg" #No Game No Life - Sora + Shiro
    "https://images7.alphacoders.com/824/824430.png" #Dragon Maid - Tohru with blossom tree
    "https://images6.alphacoders.com/884/884863.png" #Dragon Maid - Tohru running to hug Kobayashi - minimal
    "https://images5.alphacoders.com/838/838755.png" #Dragon Maid - Kobayashi at computer
    "https://images8.alphacoders.com/896/896525.png" #Dragon Maid - Tohru against flat blue - minimal
    "https://i.imgur.com/GyEmLSt.png" #Saber crying - Fate/Stay Night Unlimited Blade Works
    "https://i.imgur.com/eGmhupX.jpg" #Tohsaka Rin + Emiya carrying groceries
    "https://orig00.deviantart.net/fce8/f/2018/166/6/5/_wallpaper_engine__fate_grand_order___astolfo__2_by_xroulen-dc61zyu.jpg" #Astolfo!
    "https://i.imgur.com/8A0I305.jpg" #Bird and building girl

    #### BEGIN STEINS;GATE DUMP from https://imgur.com/gallery/dARAq ####
    "https://i.imgur.com/HxyRvFs.jpg" #01 Suzuha
    "https://i.imgur.com/oy4rWPF.png" #02 Mayushii and Kirusu catgirls
    #03 [SKIP] Kirisu undressing
    "https://i.imgur.com/2goi1rY.jpg" #04 Suzuha
    "https://i.imgur.com/D5q4JbH.png" #05 Characters against grey background
    #06 [SKIP] Kirisu and Dr Pepper
    "https://i.imgur.com/2egY72a.png" #07 Characters labeled 1 - 8
    "https://i.imgur.com/15gSx7M.png" #08 Okabe and Kirisu against dark background
    "https://i.imgur.com/4Qzj8sH.jpg" #09 Kirisu and Okabe
    "https://i.imgur.com/ian9cxa.png" #10 Kirisu and 1.048596 "It is the choice of Steins;Gate"
    "https://i.imgur.com/Xccxq01.jpg" #11 Kirisu with hair fading to nixie tubes
    #12 [SKIP] Mayushii's stopwatch, and lots of it
    "https://i.imgur.com/zufiviu.png" #13 Minimalistic Okabe reaching out
    "https://i.imgur.com/7JjLqZA.jpg" #14 Mayushii reaching out against checked background
    #15 [SKIP] Christmas Mayushii and Kirisu
    "https://i.imgur.com/ewkfpbf.jpg" #16 Kirisu against white background
    "https://i.imgur.com/1poKcCK.jpg" #17 Mayushii against white background
    #18 [SKIP] Mayushii holding a banana
    "https://i.imgur.com/gq3FqES.jpg" #19 Kirusu against white background with gear shadows
    "https://i.imgur.com/OAdCOeW.jpg" #20 Dark Kirisu and Okabe with 1.130209 in nixie tubes
    "https://i.imgur.com/LmdCyvR.jpg" #21 Kirisu and Mayushii watching fireworks
    "https://i.imgur.com/iypXsOh.png" #22 Suzuha against white background
    "https://i.imgur.com/OjZHENl.jpg" #23 Kirisu against plain background, heavily vignetted
    "https://i.imgur.com/LxWDL6f.jpg" #24 Kirisu posing with Okabe on his phone
    "https://i.imgur.com/U18vZXx.jpg" #25 Faris on the bed
    "https://i.imgur.com/NlQJmbD.png" #26 Minimalistic Kirisu posing
    #27 [SKIP: Too low res] Minimalistic Mayushii
    "https://i.imgur.com/8jA8Jic.jpg" #28 Mayushii reaching out
    "https://i.imgur.com/hwa4sne.png" #29 Kirisu on cough with "STEINS; GATE" text
    #30 [SKIP] Minimalistic Moeka
    "https://i.imgur.com/OOK1cQf.jpg" #31 Suzuha against plain background, heavily vignetted
    #32 [SKIP] Character silhouettes against blue background, with Kirisu posing
    "https://i.imgur.com/AyKzwcX.png" #33 Minimalistic Kirisu hugging Okabe
    "https://i.imgur.com/Qm4iVxE.png" #34 Minimalistic Okabe on phone
    "https://i.imgur.com/7lHQ76I.png" #35 Minimalistic Okabe with flowing lab coat
    "https://i.imgur.com/D4ukdgo.png" #36 Kirisu and Okabe with blue/red artsy vibe
    "https://i.imgur.com/bFmcVXT.jpg" #37 Faris, Mayishii, Kirisu, and Ruka against white background
    "https://i.imgur.com/KUesvSk.jpg" #38 Kirisu and Mayushii in foreground, Daru in the background
    "https://i.imgur.com/Jhp0dfb.jpg" #39 Kirisu x3 in the library
    "https://i.imgur.com/RuyxwTV.png" #40 Minimalistic Ruka
    "https://i.imgur.com/uEUuivx.png" #41 Minimalistic Faris
    "https://i.imgur.com/rfB90UW.jpg" #42 Kirisu and Mayushii against white background in new year's attire
    "https://i.imgur.com/lw3SP3P.jpg" #43 Okabe and Kirisu posing
    "https://i.imgur.com/naWJlAp.png" #44 Minimalistic Suzuha
    "https://i.imgur.com/2o9HmJL.jpg" #45 Minimalistic Kirisu and Okabe
    "https://i.imgur.com/09A5x34.png" #46 Faris and Kirisu seated
    "https://i.imgur.com/5FdN87s.jpg" #47 Kirisu and Faris against white background
    "https://i.imgur.com/uUWzPwy.png" #48 Kirisu against blue grid
    "https://i.imgur.com/XLszCFd.jpg" #49 Kirisu pointing finger out
    #50 [SKIP] Sketched Kirisu
    "https://i.imgur.com/4Y8n3e7.png" #51 Characters against a blue background
    "https://i.imgur.com/LRF9TH6.png" #52 Kirisu and Okabe against gears and time machine
    #53 [SKIP] Mayushii and Kirisu in bikinis
    #54 [SKIP] Kirisu lying down in raid
    #55 [SKIP] Chibi Kirisu holding Dr Pepper against white background
    #### END STEINS;GATE DUMP from https://imgur.com/gallery/dARAq ####

    "https://i.redd.it/n73uk4ihnafz.png" #Amadeus Kirisu
    "https://images6.alphacoders.com/785/785425.png" #Rin Tohsaka and Archer, with other servants in the background
    "http://jonvilma.com/images/fate-stay-night-3.jpg" #Saber against clouds
    "http://jonvilma.com/images/fate-stay-night-13.jpg" #Rin Tohsaka and Archer in sword reality marble
    "https://wallpaperstudio10.com/static/wpdb/wallpapers/1920x1080/174035.jpg" #Saber shouting "Excalibur", probably
    "https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-421919.jpg" #Cirno on the beach holding chibi Cirno
    "https://i.redd.it/gclmsr8yezh21.png" #Minimalist Kurisu and Okabe on a white background
)

FILECOUNT=$(ls -l "$DIR/backgrounds/"* | wc -l)

if [ $FILECOUNT -eq ${#IMAGES[@]} ]; then
    #The number of files equals the number of URLs
    #So chances are all the backgrounds are there and downloaded
    #So get outta here
    exit
fi

#Redownload everything
mkdir -p "$DIR/backgrounds"
rm "$DIR/backgrounds/"*

i=0
for url in "${IMAGES[@]}"; do
    notify-send "Downloading background $((i + 1))/${#IMAGES[@]}" "$DIR/backgrounds/${url//\//_}"
    echo "Downloading: " $url
    curl "$url" -o "$DIR/backgrounds/${url//\//_}"

    ((i++))
done

notify-send "Finished downloading backgrounds"
