# Kudos to https://github.com/caiogondim/bullet-train.zsh for some of the framework here

CURRENT_BG='NONE'
SEGMENT_SEPARATOR=''

# Check if we're connected remotely
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
    SESSION_TYPE=remote/ssh
else
    case $(ps -o comm= -p $PPID) in
        sshd|*/sshd) SESSION_TYPE=remote/ssh;;
    esac
fi

# Begin a segment
# Takes three arguments, background, foreground and text. All of them can be omitted,
# rendering default background/foreground and no text.
prompt_segment() {
    local bg fg
    [[ -n $1 ]] && bg="%K{$1}" || bg="%k"
    [[ -n $2 ]] && fg="%F{$2}" || fg="%f"
    if [[ $CURRENT_BG != 'NONE' && $1 != $CURRENT_BG ]]; then
        echo -n " %{$bg%F{$CURRENT_BG}%}${SEGMENT_SEPARATOR}%{$fg%} "
    else
        echo -n "%{$bg%}%{$fg%} "
    fi
    CURRENT_BG=$1
    [[ -n $3 ]] && echo -n $3
}

# End the prompt, closing any open segments
prompt_end() {
    if [[ -n $CURRENT_BG ]]; then
        echo -n " %{%k%F{$CURRENT_BG}%}$SEGMENT_SEPARATOR"
    else
        echo -n "%{%k%}"
    fi
    echo -n "%{%f%}"
    CURRENT_BG=''
}

reset_prompt() {
    zle reset-prompt
}

#########################################################################################################################

prompt_module_init_time() {}
prompt_module_start_time() {}
prompt_module_get_time() {
    prompt_segment white black '%D{%H:%M:%S}'
}

# Status:
# - was there an error
# - am I root
# - are there background jobs?
prompt_module_init_status() {}
prompt_module_start_status() {}
prompt_module_get_status() {
    local symbols
    symbols=()
    [[ $RETVAL -ne 0 ]] && symbols+="✘ $RETVAL"
    [[ $UID -eq 0 ]] && symbols+="%{%F{yellow}%}⚡%f"
    [[ $(jobs -l | wc -l) -gt 0 ]] && symbols+="⚙"

    if [[ -n "$symbols" && $RETVAL -ne 0 ]]; then
        prompt_segment red white "$symbols"
    elif [[ -n "$symbols" ]]; then
        prompt_segment green black "$symbols"
    fi
}

prompt_module_init_context() {}
prompt_module_start_context() {}
if [[ $SESSION_TYPE == "remote/"* ]]; then
    prompt_module_get_context() {
        prompt_segment magenta white '%n@%m'
    }
else
    prompt_module_get_context() {
        prompt_segment black gray '%n@%m'
    }
fi

prompt_module_init_path() {}
prompt_module_start_path() {}
prompt_module_get_path() {
    prompt_segment blue black '%4(c:...:)%3c'
}

if [ ! -n "${BULLETTRAIN_GIT_PREFIX+1}" ]; then
    ZSH_THEME_GIT_PROMPT_PREFIX="\ue0a0 "
else
    ZSH_THEME_GIT_PROMPT_PREFIX=$BULLETTRAIN_GIT_PREFIX
fi
if [ ! -n "${BULLETTRAIN_GIT_SUFFIX+1}" ]; then
    ZSH_THEME_GIT_PROMPT_SUFFIX=""
else
    ZSH_THEME_GIT_PROMPT_SUFFIX=$BULLETTRAIN_GIT_SUFFIX
fi
if [ ! -n "${BULLETTRAIN_GIT_DIRTY+1}" ]; then
    ZSH_THEME_GIT_PROMPT_DIRTY=" %F{red}✘%F{black}"
else
    ZSH_THEME_GIT_PROMPT_DIRTY=$BULLETTRAIN_GIT_DIRTY
fi
if [ ! -n "${BULLETTRAIN_GIT_CLEAN+1}" ]; then
    ZSH_THEME_GIT_PROMPT_CLEAN=" %F{green}✔%F{black}"
else
    ZSH_THEME_GIT_PROMPT_CLEAN=$BULLETTRAIN_GIT_CLEAN
fi
if [ ! -n "${BULLETTRAIN_GIT_ADDED+1}" ]; then
    ZSH_THEME_GIT_PROMPT_ADDED=" %F{green}✚%F{black}"
else
    ZSH_THEME_GIT_PROMPT_ADDED=$BULLETTRAIN_GIT_ADDED
fi
if [ ! -n "${BULLETTRAIN_GIT_MODIFIED+1}" ]; then
    ZSH_THEME_GIT_PROMPT_MODIFIED=" %F{blue}✹%F{black}"
else
    ZSH_THEME_GIT_PROMPT_MODIFIED=$BULLETTRAIN_GIT_MODIFIED
fi
if [ ! -n "${BULLETTRAIN_GIT_DELETED+1}" ]; then
    ZSH_THEME_GIT_PROMPT_DELETED=" %F{red}✖%F{black}"
else
    ZSH_THEME_GIT_PROMPT_DELETED=$BULLETTRAIN_GIT_DELETED
fi
if [ ! -n "${BULLETTRAIN_GIT_UNTRACKED+1}" ]; then
    ZSH_THEME_GIT_PROMPT_UNTRACKED=" %F{yellow}✭%F{black}"
else
    ZSH_THEME_GIT_PROMPT_UNTRACKED=$BULLETTRAIN_GIT_UNTRACKED
fi
if [ ! -n "${BULLETTRAIN_GIT_RENAMED+1}" ]; then
    ZSH_THEME_GIT_PROMPT_RENAMED=" ➜"
else
    ZSH_THEME_GIT_PROMPT_RENAMED=$BULLETTRAIN_GIT_RENAMED
fi
if [ ! -n "${BULLETTRAIN_GIT_UNMERGED+1}" ]; then
    ZSH_THEME_GIT_PROMPT_UNMERGED=" ═"
else
    ZSH_THEME_GIT_PROMPT_UNMERGED=$BULLETTRAIN_GIT_UNMERGED
fi
if [ ! -n "${BULLETTRAIN_GIT_AHEAD+1}" ]; then
    ZSH_THEME_GIT_PROMPT_AHEAD=" ⬆"
else
    ZSH_THEME_GIT_PROMPT_AHEAD=$BULLETTRAIN_GIT_AHEAD
fi
if [ ! -n "${BULLETTRAIN_GIT_BEHIND+1}" ]; then
    ZSH_THEME_GIT_PROMPT_BEHIND=" ⬇"
else
    ZSH_THEME_GIT_PROMPT_BEHIND=$BULLETTRAIN_GIT_BEHIND
fi
if [ ! -n "${BULLETTRAIN_GIT_DIVERGED+1}" ]; then
    ZSH_THEME_GIT_PROMPT_DIVERGED=" ⬍"
else
    ZSH_THEME_GIT_PROMPT_DIVERGED=$BULLETTRAIN_GIT_PROMPT_DIVERGED
fi
prompt_module_init_git() {
    async_start_worker prompt_module_worker_git
    async_register_callback prompt_module_worker_git prompt_module_worker_git_completed

    PROMPT_MODULE_GIT_STATUS_TEXT=""
    PROMPT_MODULE_GIT_BG_COLOR=""
    PROMPT_MODULE_GIT_FG_COLOR=""
}
prompt_module_start_git() {
    async_flush_jobs prompt_module_worker_git
    async_job prompt_module_worker_git prompt_module_async_git "$(pwd)"
}
prompt_module_async_git() {
    cd "$1"

    local ref dirty mode repo_path
    repo_path=$(git rev-parse --git-dir 2>/dev/null)

    if $(git -C "$cwd" rev-parse --is-inside-work-tree >/dev/null 2>&1); then
        if [[ -n $(git status --porcelain --ignore-submodules) ]]; then
            echo "yellow"
            echo "black"
        else
            echo "green"
            echo "black"
        fi

        echo "$(git_prompt_info)$(git_prompt_status)"
    fi
}
prompt_module_worker_git_completed() {
    local elems
    elems=("${(f)3}")

    PROMPT_MODULE_GIT_BG_COLOR="${elems[1]}"
    PROMPT_MODULE_GIT_FG_COLOR="${elems[2]}"
    PROMPT_MODULE_GIT_STATUS_TEXT="${elems[3]}"

    reset_prompt
}
prompt_module_get_git() {
    if [ ! -z "$PROMPT_MODULE_GIT_STATUS_TEXT" ]; then
        prompt_segment "$PROMPT_MODULE_GIT_BG_COLOR" "$PROMPT_MODULE_GIT_FG_COLOR" "$PROMPT_MODULE_GIT_STATUS_TEXT"
    fi
}

format_time() {
    local T=$1
    local D=$((T/60/60/24))
    local H=$((T/60/60%24))
    local M=$((T/60%60))
    local S=$((T%60))
    [[ $D > 0 ]] && printf '%dd' $D
    [[ $H > 0 ]] && printf '%dh' $H
    [[ $M > 0 ]] && printf '%dm' $M
    printf '%ds' $S
}

prompt_module_init_exec_time() {}
prompt_module_start_exec_time() {}
prompt_module_get_exec_time() {
    [ $BULLETTRAIN_last_exec_duration -gt 0 ] && prompt_segment black gray "$(format_time $BULLETTRAIN_last_exec_duration)"
}

PROMPT_MODULES=(
    time
    status
    context
    path
    git
    exec_time
)

for segment in $PROMPT_MODULES; do
    prompt_module_init_$segment
done

preexec() {
    cmd_timestamp=`date +%s`
}

precmd() {
    RETVAL="$?"

    local stop=`date +%s`
    local start=${cmd_timestamp:-$stop}
    let BULLETTRAIN_last_exec_duration=$stop-$start
    cmd_timestamp=''

    for segment in $PROMPT_MODULES; do
        prompt_module_start_$segment
    done
}

build_prompt() {
    for segment in $PROMPT_MODULES; do
        prompt_module_get_$segment
    done
    prompt_end
}

PROMPT='
$(build_prompt)
%F{green}%(!.#.$)%F{reset} '

# TMOUT=1
# TRAPALRM() {
#     zle reset-prompt
# }
